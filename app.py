import os
import flask


app = flask.Flask(__name__)

@app.route("/")
def main_page():
    return "Hello OpenShift"

app.run(port=8888,host="0.0.0.0")